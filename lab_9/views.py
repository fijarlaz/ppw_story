from django.shortcuts import render
from django.http import JsonResponse
import urllib.request, json
import requests
from django.conf import settings
from django.contrib.auth import logout
from django.shortcuts import redirect
from django.contrib.auth import authenticate
from django.views.decorators.csrf import csrf_exempt

# Create your views here.
def index(request):
    
    return render(request, 'index.html')

def book_data(request, query_search):
    if not query_search:
        raw_data = requests.get('https://www.googleapis.com/books/v1/volumes?q=quilting').json()
    else:
        raw_data = requests.get('https://www.googleapis.com/books/v1/volumes?q='+query_search).json()
    itemss =[]
    for info in raw_data['items']:
        data = {}
        if info['volumeInfo']['title']:
            data['title'] = info['volumeInfo']['title']
            data['author'] = ", ".join(info['volumeInfo']['authors'])
        if info['volumeInfo']['publishedDate']:
            data['publishedDate'] = info['volumeInfo']['publishedDate']
        itemss.append(data)
    return JsonResponse({"data" :itemss})

