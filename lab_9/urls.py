from django.urls import path,include
from .views import index, book_data
from django.conf.urls import url
from django.contrib.auth import views
from django.conf import settings

urlpatterns= [
    path('', index, name='index'),
    url(r'^api/books/(?P<query_search>\w+)/$', book_data),
    url(r'^login/$', views.LoginView.as_view(), name='login'),
    url(r'^logout/$', views.LogoutView.as_view(), {'next_page': settings.LOGOUT_REDIRECT_URL}, name='logout'),
    url(r'^auth/', include('social_django.urls', namespace='social')),
]