from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from .views import index, book_data
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
# Create your tests here.
    # Create your tests here.
class Story6UnitTest(TestCase):

    def test_story_9_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)

    def test_story_9_hello_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'index.html')
    
    def test_story_9_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)
    
    # def test_story_9_using_data_func(self, val):
    #     found = resolve('/api/books/'+val)
    #     self.assertEqual(found.func, books)


# class Lab9_FunctionalTest(LiveServerTestCase):
#     def setUp(self):
#         chrome_options = Options()
#         chrome_options.add_argument('--dns-prefetch-disable')
#         chrome_options.add_argument('--no-sandbox')
#         chrome_options.add_argument('--headless')
#         chrome_options.add_argument('disable-gpu')
#         chrome_options.add_argument('--disable-dev-shm-usage')
#         service_log_path = "./chromedriver.log"
#         service_args = ['--verbose']
#         self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
#         self.selenium.implicitly_wait(25)
#         super(Lab9_FunctionalTest, self).setUp()

#     def tearDown(self):
#         self.selenium.quit()
#         super(Lab9_FunctionalTest, self).tearDown()

#     def test_layout_page_title(self):
#         selenium = self.selenium
#         selenium.get('http://ppw-fijar.herokuapp.com/')
#         self.assertIn('Library', self.selenium.title)
#         time.sleep(2)
