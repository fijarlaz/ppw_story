from django import forms
from .models import Subscriber

class SubscriberForm(forms.Form):
	pass_attrs= {
		'class' : 'form-control',
		'id' : 'pass-form',
		'placeholder' : 'Password',
	}
	email_attrs = {
		'class' : 'form-control',
		'id' : 'email-form',
		'placeholder' : 'email@example.com'
	}
	nama_attrs = {
		'class' : 'form-control',
		'id' : 'name-form',
		'placeholder' : 'Input Your Name'

	}
	name = forms.CharField(max_length=60, widget=forms.TextInput(attrs=nama_attrs), label='Name')
	email = forms.EmailField(widget=forms.EmailInput(attrs=email_attrs), label='Email')
	passowrd = forms.CharField(max_length = 50,widget=forms.PasswordInput(attrs=pass_attrs), label='Password')