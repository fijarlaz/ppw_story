from django.shortcuts import render, redirect
from .forms import SubscriberForm
from .models import Subscriber
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse, HttpResponse
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from django.core.serializers import serialize
from django.db import IntegrityError
import requests
# Create your views here.


response={}
@csrf_exempt
def profile(request):
	all_subs = Subscriber.objects.all()
	if request.method =='POST':
		form= SubscriberForm(request.POST or None)
		name = request.POST['name']
		email = request.POST['email']
		password= request.POST['password']

		subs_filter = Subscriber.objects.filter(email =email)

		if (len(subs_filter)>0):
			response_message= 'Email has been taken'
			# print(response_message)
			print('nga bisa')
			
		else:
			subs = Subscriber(name =name, email = email, password=password)
			subs.save()

	else:
		form = SubscriberForm()

	return render(request, 'profile.html', {'subs':all_subs, 'form':form})


@csrf_exempt
def check_email(request):
	if request.method=='POST':
		name = request.POST['name']
		email= request.POST['email']
		password = request.POST['password']

		subs_filter = Subscriber.objects.filter(email = email)

		if len(name)==0 or len(name)>30:
			return JsonResponse({'message':'Name not valid'})


		try:
			validate_email(email)

		except Exception as e:
			return JsonResponse({'message': 'Email not valid'})
		if len(subs_filter)>0:
			return JsonResponse({'message': 'Email already used'})

		if len(password)<8 :
			return JsonResponse({'message': 'Password not valid'})
		else:
			return JsonResponse({'message' : 'User valid'})
	else:
		return JsonResponse({'message': 'Something wrong'})


def subs_list(request):
	all_subs = Subscriber.objects.all().values()
	subs = list(all_subs)
	# all_subs = serialize('json', Subscriber.objects.all())
	# subscriber ={'list' : all_subs}
	return JsonResponse({'all_subs':subs}, safe=False)
	
@csrf_exempt
def unsubscribe(request):
	if request.method =='POST':
		idnya = request.POST['id']
		subs  = Subscriber.objects.filter(id=idnya).delete()
		return render(request, 'profile.html')