from django.urls import path
from . import views

urlpatterns = [
	path('', views.profile, name = 'profile'),
	path('check-email/', views.check_email, name='check-email'),
	path('api/subs/', views.subs_list, name='subscriber'),
	path('delete-subscriber/', views.unsubscribe, name='unsubscribe')
]